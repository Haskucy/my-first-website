from django.db import models

class PortofolioProyek(models.Model):
    Judul_Proyek = models.CharField(max_length=250)
    Dari_Tanggal = models.DateField()
    Sampai_Tanggal = models.DateField()
    Deskripsi_Proyek = models.CharField(max_length=1000)
    Tempat_Proyek = models.CharField(max_length=250)
    Kategori_Proyek = models.CharField(max_length=100)
