from django.urls import *
from .views import index, kontak
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^kontak/', kontak, name='kontak')
]
