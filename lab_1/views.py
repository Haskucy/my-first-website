from django.shortcuts import render
from django.utils.timezone import datetime #important if using timezones
from .forms import PostForm
import datetime

def index(request):
    return render(request, 'Index.html')
def kontak(request):
    return render(request, 'Contactme.html')

def Portofolio(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone(now)
            post.save()
    else:
        form = PostForm()
    return render(request, 'Portofolio.html', {'form' : form})
