from django import forms
from .models import PortofolioProyek

class PostForm(forms.ModelForm):
    class Meta:
        model = PortofolioProyek
        fields = ('Judul_Proyek', 'Dari_Tanggal',
        'Sampai_Tanggal', 'Deskripsi_Proyek',
        'Tempat_Proyek','Kategori_Proyek')
