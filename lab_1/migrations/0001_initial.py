# Generated by Django 2.2a1 on 2019-03-07 08:26

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PortofolioProyek',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Judul_Proyek', models.CharField(max_length=250)),
                ('Dari_Sampai', models.CharField(max_length=500)),
                ('Deskripsi_Proyek', models.CharField(max_length=1000)),
                ('Tempat_Proyek', models.CharField(max_length=250)),
                ('Kategori_Proyek', models.CharField(max_length=100)),
            ],
        ),
    ]
